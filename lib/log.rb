require 'yaml'

class Log < Array

  def initialize(storage)
    @storage = storage
    data = @storage.load
    self.replace(data) if data.is_a? Array
  end


  def << entry
    super(entry)
    @storage.save self
  end

end
