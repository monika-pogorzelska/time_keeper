require 'time'

class LogEntryView
  def self.print(log_entry, duration)
    start_time = log_entry.pretty_start_time
    tags = log_entry.pretty_tags
    description = log_entry.pretty_description
    entry_duration = pretty_duration(duration)
    "#{start_time} (#{entry_duration}) #{log_entry.type} #{tags} #{description}"
  end

  def self.pretty_duration(duration)
    hours = duration / (60 * 60)
    minutes = (duration / 60) % 60
    '%02d:%02d' % [hours, minutes]     
  end
end