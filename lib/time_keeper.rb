require_relative 'log_entry.rb'
require_relative 'log.rb'
require_relative 'yaml_storage.rb'
require_relative 'txt_storage.rb'
require_relative 'log_entry_view.rb'
require_relative 'log_entry_summary.rb'

class TimeKeeper
  if ENV["XDG_DATA_HOME"]
    DATA_DIR = "#{ENV["XDG_DATA_HOME"]}/time_keeper"
  else
    DATA_DIR = "#{ENV["HOME"]}/.time_keeper"
  end

  def initialize
    Dir.mkdir DATA_DIR unless Dir.exist? DATA_DIR
    # storage = YAMLStorage.new("#{ENV['HOME']}/projects/learn_to_code/time_keeper/time_log.yaml")
    storage = TXTStorage.new("#{DATA_DIR}/time_log.txt")
    @log = Log.new(storage)
  end

  def run
    loop do
      print_log
      puts 'Add new record or type "q" to quit or "r" to reload'
      puts 'Enter start time (emp ty - use current)'
      puts 'Date and time: "YYYY-MM-DD HH:MM" or time only "HH:MM" (current date)'
      user_input = STDIN.gets.chomp!
      break if user_input == 'q'
      if user_input != 'r'
        if user_input.empty?
          start_time = nil
        else
          start_time = user_input
        end
        puts 'Enter type number'
        LogEntry::TYPES.each_with_index do |type, no|
          puts "#{no} - #{type}"
        end
        type_no = STDIN.gets.chomp.to_i
        type = LogEntry::TYPES[type_no]
        puts 'Enter tags, split by ";", empty - no tags'
        tags = STDIN.gets.chomp.split(';').map { |tag| tag.strip }
        puts 'Enter description (empty - no description)'
        user_input = STDIN.gets.chomp
        if user_input.empty?
          description = nil
        else
          description = user_input
        end

        begin
          @log << LogEntry.new(type, start_time, description, *tags)
        rescue LogEntry::LogEntryError => e
          puts e.message
        end
      end
    end
  end

  def range_summary(range, print_entries = false)
    summary = LogEntrySummary.new
    range.each do |index|
      end_time = @log[index + 1] ? @log[index + 1].start_time : nil
      duration = ((end_time || Time.now) - @log[index].start_time)
      type = @log[index].type
      summary.add_duration(type, duration)
      puts LogEntryView.print(@log[index], duration) if print_entries
    end
    summary.print
    puts
  end

  def print_log
    return unless @log.any?
    indicies_sleep = @log.each_index.select do |i| 
      @log[i].type == 'sleep' && @log[i].tags.any? {|tag| tag == 'night'}
    end
    index_last_sleep = indicies_sleep.last
    current_day_start = index_last_sleep ? index_last_sleep + 1 : 0
    last_index = @log.size
    print_entries = true
    if ARGV.empty?
      start = -2
      print_entries = false
    else
      if ARGV[0].to_i > indicies_sleep.size
        start = -indicies_sleep.size
      else
        start = -ARGV[0].to_i
      end
    end
    if indicies_sleep.size > 1
      for n in start...-1
        range_summary((indicies_sleep[n]+1)..indicies_sleep[n + 1], print_entries)
      end
    elsif indicies_sleep.size == 1
      range_summary(0..indicies_sleep[0])
    end
    puts "Current log:"
    range_summary(current_day_start...last_index, true)
  end
end
