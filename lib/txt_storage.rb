class TXTStorage
  def initialize(path)
    @path = path
  end

  def load
    data =nil
    File.open(@path, 'w') {} unless File.exist? @path
    File.open(@path,'r') do |file|
      data = file.each_line.map {|line| create_entry(line)}
    end
    data
  end 

  def save(enum_object)
    File.open(@path, 'w') do |file|
      file.write enum_object.map { |i| i.to_s }.join("\n")
    end
  end

  private

  def create_entry(line)
    values = parse_line(line)
    LogEntry.new(
      values[:type], values[:start_time], values[:description], *values[:tags]
    )
  end

  def parse_line(line)
    vals = line.match %r{
      (?<time>\d{4}-\d{2}-\d{2}[[:space:]]\d{2}\:\d{2})
      [[:space:]]
      (?<type>\w+)
      [[:space:]]
      (\[
        (?<tags>.+)
      \])?
      [[:space:]]
      ("
        (?<description>.+)
      ")?
    }x
    type        = vals['type']
    start_time  = vals['time']
    tags        = vals['tags'] && vals['tags'].split('; ')
    description = vals['description']
    { type: type, start_time: start_time, tags: tags, description: description }
  end

end