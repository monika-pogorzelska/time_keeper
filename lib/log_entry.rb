require 'time'

class LogEntry

  class LogEntryError < RuntimeError
  end

  class InvalidType < LogEntryError
  end

  TYPES = [
    'personal',
    'work',
    'recreation',
    'education',
    'routines',
    'relax',
    'other',
    'sleep',
  ]

  attr_reader :start_time, :type, :description, :tags

  def initialize(type, start_time = nil, description = nil, *tags)
    if TYPES.include? type
      @type = type
    else
      raise InvalidType, "Type '#{type.inspect}' is invalid."
    end
    if start_time
      @start_time = Time.parse start_time
    else
      @start_time = Time.new
    end
    @description = description
    @tags = tags
  end

  def to_s
    "#{pretty_start_time} #{@type} #{pretty_tags} #{pretty_description}"
  end

  def pretty_start_time
    @start_time.strftime('%Y-%m-%d %H:%M')
  end

  def pretty_tags
    if @tags.any?
      "[#{@tags.join('; ')}]"
    else
      ''
    end
  end

  def pretty_description
    if @description
      "\"#{@description}\""
    else
      ''
    end
  end

end

