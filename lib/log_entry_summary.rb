class LogEntrySummary

  def initialize()
    @types = {
      'personal' => 0,
      'work' => 0,
      'recreation' => 0,
      'education' => 0,
      'routines' => 0,
      'relax' => 0,
      'other' => 0,
      'sleep' => 0,
    }
  end
  
  def add_duration(type, duration)
    @types[type] += duration
  end

  def print
    puts "-" * 80
    summary = @types.map do |key, value|
      "#{key}: #{pretty_duration(value)}"
    end
    Kernel.print summary.join(' | ')
    puts
  end

  def pretty_duration(duration)
    hours = duration / (60 * 60)
    minutes = (duration / 60) % 60
    '%02d:%02d' % [hours, minutes]     
  end
end