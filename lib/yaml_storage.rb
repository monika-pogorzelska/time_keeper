require 'yaml'

class YAMLStorage
  def initialize(path)
    @path = path
  end

  def load
    data = nil
    File.open(@path, 'w') {} unless File.exist? @path
    File.open(@path, 'r') do |file|
      data = YAML.load file.read
    end
    return data
  end

  def save(object)
    File.open(@path, 'w') { |file| file.write object.to_yaml }
  end
  
end